��          �      L      �  ,   �     �                '     4  &   D     k  #   �     �     �     �     �  
          )        C     [  �  s  *   �     *     J     h     p     ~  A   �      �  ,   �     "     5  ;   Q      �     �     �  )   �     �     �                         
      	                                                                     Adds holiday mode options to ClassicCommerce Change to backorders ClassicCommerce Holiday Mode Enable Holiday Mode Holiday message Make all holiday purchases backorders. Mark site as in vacation mode. Prevent purchases while on holiday. Prevent sales Save settings We can't take orders right now. What to show customers. booleanNo booleanYes https://gitlab.com/octaid/cc-holiday-mode https://www.octaid.com/ indefinitedevil, Octaid Project-Id-Version: ClassicCommerce Holiday Mode 1.1
Report-Msgid-Bugs-To: https://gitlab.com/octaid/cc-holiday-mode
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-05-12 16:08+0200
X-Generator: Poedit 2.3
X-Domain: cc-holiday-mode
Last-Translator: Octaid
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Ajoute un mode vacances à ClassicCommerce Passer les commandes en attente Mode Vacances ClassicCommerce Activer Mode Vacances Message d’absence Mettre toutes les commandes passées en Mode Vacances en attente. Mettre le site en Mode Vacances. Bloquer les achats pendant le Mode Vacances. Bloquer les ventes Sauvegarder les paramètres Nous ne pouvons pas recevoir de commandes pour l’instant. Ce qui est affiché aux clients. Non Oui https://gitlab.com/octaid/cc-holiday-mode https://www.octaid.com/ indefinitedevil, Octaid 